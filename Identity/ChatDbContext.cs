﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace chat_application.Identity
{
    public class ChatDbContext : IdentityDbContext<AppIdentityUser, AppIdentityRole, string>
    {
        public ChatDbContext(DbContextOptions<ChatDbContext> options) : base(options)
        {

        }
    }
}
